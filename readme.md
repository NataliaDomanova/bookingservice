# Booking Room Service

Booking Room Service is a mock service providing
functionalities to manage employees, conference rooms and rooms bookings in an office with in-memory database via RESTful API.


## Getting Started


### Prerequisites

What things you need to install the software and how to install them

```
JDK 1.8
Maven 3
```

### Installing

One of the ways to run Booking Room Service is to execute the main method in the com.ndomanova.bookingservice.BookingServiceApplication from the IDE.

Alternatively you can ru it from the terminal

```bash
$ mvn clean install
$ mvn spring-boot:run
```

## Requests descriptions

| HTTP METHOD           | POST            | GET           | PUT         | DELETE |
| -----------           | --------------- | ---------     | ----------- | ------ |
| CRUD OP               | CREATE          | READ          | UPDATE      | DELETE |
| /users                | Create new user | List of users | Error       | Error  |
| /users?login    | Error           | Error         | If exists, update user; If not, error | If exists, delete user; If not, error |
| /rooms    			| Create new room | List of rooms | Error | Error |
| /rooms?name   	| Error           | Error         | If exists, update room; If not, error | If exists, delete room; If not, error |
| /booking?start_time&endt_time| Error | List of booking in time frame | Error | Error |
| /booking?room&start_time&endt_time| Error | List of booking of room in time frame | Error | Error |
| /booking?name&surname&start_time&endt_time| Error | List of user's booking in time frame | Error | Error |
| /booking?loginn&password&room&start_time&endt_time| Create new booking | Error | Error | Error |

### Examples
#### GET
Input:
```
/booking?start_time=2002-01-01 10:10:10&end_time=2002-05-02 11:10:10
```

Output:

```http
HTTP/1.1 200 OK
Date: Mon Apr 22 10:56:15 CEST 2019
Content-Type: application/json

[{"name":"John",
  "surname":"Smith",
  "room":"Medium Room",
  "startTime":"2002-02-02T09:10:10.000+0000",
  "endTime":"2002-02-02T10:10:10.000+0000"
  },
  {
  "name":"John",
  "surname":"Smith",
  "room":"Small Room",
  "startTime":"2002-03-02T13:10:10.000+0000",
  "endTime":"2002-03-02T16:10:10.000+0000"
  },
  {"name":"Jane",
  "surname":"Doe",
  "room":"Large Room",
  "startTime":"2002-02-02T09:10:10.000+0000",
  "endTime":"2002-02-02T10:10:10.000+0000"
  }
]
```
#### PUT
Input:
```
"/rooms?name=Small Room"
```

Output:

```http
HTTP/1.1 200 OK
Date: Mon Apr 22 10:56:15 CEST 2019
Content-Type: MediaType

"room is updated successfully"
```
#### DELETE
#### Post
Input:
```
/users?login=ndomanova
```

Output:

```http
HTTP/1.1 400 BAD REQUEST
Date: Mon Apr 22 10:56:15 CEST 2019
Content-Type: MediaType

"user is not found"
```
## Authors

* **Natalia Domanova**

