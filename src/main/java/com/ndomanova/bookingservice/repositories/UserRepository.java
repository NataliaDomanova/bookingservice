package com.ndomanova.bookingservice.repositories;

import com.ndomanova.bookingservice.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {
    User findByLogin(String login);
    List<User> findAll();
}
