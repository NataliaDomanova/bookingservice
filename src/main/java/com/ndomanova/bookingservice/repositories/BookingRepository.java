package com.ndomanova.bookingservice.repositories;

import com.ndomanova.bookingservice.entities.Booking;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface BookingRepository extends JpaRepository<Booking, Integer> {
}
