package com.ndomanova.bookingservice.repositories;

import com.ndomanova.bookingservice.entities.BookingDescription;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.awt.print.Book;
import java.util.Date;
import java.util.List;

@Repository
public interface BookingDescriptionRepository extends JpaRepository<BookingDescription, Integer> {

    List<BookingDescription> findByRoomAndStartTimeBetweenAndEndTimeBetween(String room, Date startTime1, Date endTime1,
                                                                              Date startTime2, Date endTime2);

    List<BookingDescription> findByStartTimeBetweenAndEndTimeBetween(Date startTime1, Date endTime1, Date startTime2,
                                                                 Date endTime2);

    List<BookingDescription> findByNameAndSurnameAndStartTimeBetweenAndEndTimeBetween(String name, String surname,
                                                                                  Date startTime1, Date endTime1,
                                                                                  Date startTime2, Date endTime2);
}
