package com.ndomanova.bookingservice.controllers;

import com.ndomanova.bookingservice.dtos.UserDTO;
import com.ndomanova.bookingservice.entities.SimpleUser;
import com.ndomanova.bookingservice.entities.User;
import com.ndomanova.bookingservice.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
public class UserController {
    @Autowired
    UserService userService;

    @GetMapping("/users")
    public List<UserDTO> getAll() {
        return userService.findAll();
    }

    @PostMapping("/users")
    public ResponseEntity<?> addUser(@Valid @RequestBody User user) {
        if (userService.findByLogin(user.getLogin()) != null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("user is already created");
        }
        userService.add(user);
        return ResponseEntity.status(HttpStatus.CREATED).body("user is created successfully");
    }

    @PutMapping("/users")
    public ResponseEntity<?> editUser(@Valid @RequestBody User user, @RequestParam(value = "login") String login) {
        User savedUser = userService.findByLogin(login);

        if (savedUser == null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("user is not found");
        }
        userService.edit(savedUser.copyNotNullFields(user));
        return ResponseEntity.ok("user is updated successfully");
    }

    @DeleteMapping("/users")
    public ResponseEntity<?> deleteUser(@RequestParam(value = "login") String login) {
        User user = userService.findByLogin(login);

        if (user == null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("user is not found");
        }
        userService.delete(user);
        return ResponseEntity.ok("user is deleted successfully");
    }
}
