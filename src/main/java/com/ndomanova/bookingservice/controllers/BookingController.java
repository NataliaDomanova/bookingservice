package com.ndomanova.bookingservice.controllers;

import com.ndomanova.bookingservice.dtos.BookingDescriptionDTO;
import com.ndomanova.bookingservice.entities.Booking;
import com.ndomanova.bookingservice.entities.Room;
import com.ndomanova.bookingservice.entities.User;
import com.ndomanova.bookingservice.services.BookingService;
import com.ndomanova.bookingservice.services.RoomService;
import com.ndomanova.bookingservice.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

@RestController
public class BookingController {
    @Autowired
    BookingService bookingService;
    @Autowired
    RoomService roomService;
    @Autowired
    UserService userService;

    @PostMapping("/booking")
    public ResponseEntity<?> addBooking(@RequestParam Map<String, String> params) {
        String login = params.get("login");
        String password = params.get("password");
        String roomName = params.get("room");
        Date startTime;
        Date endTime;
        try {
            startTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(params.get("start_time"));
            endTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(params.get("end_time"));
        } catch (ParseException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Incorrect date format");
        }
        if (startTime.after(endTime)) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("inconsistent dates");
        }
        User user = userService.findByLogin(login);
        if (user == null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("no user found");
        }
        if (!user.getPassword().equals(password)) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("incorrect password");
        }
        Room room = roomService.findByName(roomName);
        if (room == null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("no room found");
        }
        if (bookingService.findBookings(room.getName(), endTime, startTime).size() != 0) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("room is unavailable");
        }

        Booking booking = new Booking();
        booking.setIdUser(user.getId());
        booking.setIdRoom(room.getId());
        booking.setStartTime(startTime);
        booking.setEndTime(endTime);

        bookingService.add(booking);
        return ResponseEntity.status(HttpStatus.CREATED).body("room was booked successfully");
    }

    @GetMapping("/booking")
    public List<BookingDescriptionDTO> getBookings(@RequestParam Map<String, String> params) {
        Date startTime;
        Date endTime;
        try {
            startTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(params.get("start_time"));
            endTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(params.get("end_time"));
        } catch (ParseException e) {
            return new ArrayList<>();
        }
        if (startTime.after(endTime)) {
            return new ArrayList<>();
        }
        return bookingService.findBookings(startTime, endTime);
    }

    @GetMapping("/booking/room")
    public List<BookingDescriptionDTO> getBookingsByRoom(@RequestParam Map<String, String> params) {
        String room = params.get("room");
        Date startTime;
        Date endTime;
        try {
            startTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(params.get("start_time"));
            endTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(params.get("end_time"));
        } catch (ParseException e) {
            return new ArrayList<>();
        }
        if (startTime.after(endTime)) {
            return new ArrayList<>();
        }
        return bookingService.findBookings(room, startTime, endTime);
    }

    @GetMapping("/booking/user")
    public List<BookingDescriptionDTO> getBookingsByUser(@RequestParam Map<String, String> params) {
        String name = params.get("name");
        String surname = params.get("surname");
        String date1 = params.get("start_time");
        String date2 = params.get("end_time");
        Date startTime;
        Date endTime;
        try {
            startTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(date1);
            endTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(date2);
        } catch (ParseException e) {
            return new ArrayList<>();
        }
        if (startTime.after(endTime)) {
            return new ArrayList<>();
        }
        return bookingService.findBookings(name, surname, startTime, endTime);
    }
}
