package com.ndomanova.bookingservice.controllers;

import com.ndomanova.bookingservice.dtos.RoomDTO;
import com.ndomanova.bookingservice.entities.Room;
import com.ndomanova.bookingservice.services.RoomService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
public class RoomController {
    
    @Autowired
    RoomService roomService;

    @GetMapping("/rooms")
    public List<RoomDTO> getAll() {
        return roomService.findAll();
    }

    @PostMapping("/rooms")
    public ResponseEntity<?> addRoom(@Valid @RequestBody Room room) {
        if (roomService.findByName(room.getName()) != null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("room is already created");
        }
        roomService.add(room);
        return ResponseEntity.status(HttpStatus.CREATED).body("room is created successfully");
    }

    @PutMapping("/rooms")
    public ResponseEntity<?> editRoom(@Valid @RequestBody Room room, @RequestParam(value = "name") String login) {
        Room savedRoom = roomService.findByName(login);

        if (savedRoom == null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("room is not found");
        }
        roomService.edit(savedRoom.copyNotNullFields(room));
        return ResponseEntity.ok("room is updated successfully");
    }

    @DeleteMapping("/rooms")
    public ResponseEntity<?> deleteRoom(@RequestParam(value = "name") String name) {
        Room room = roomService.findByName(name);

        if (room == null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("room is not found");
        }
        roomService.delete(room);
        return ResponseEntity.ok("room is deleted successfully");
    }
}
