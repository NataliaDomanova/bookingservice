package com.ndomanova.bookingservice.services;

import com.ndomanova.bookingservice.dtos.UserDTO;
import com.ndomanova.bookingservice.entities.SimpleUser;
import com.ndomanova.bookingservice.entities.User;
import com.ndomanova.bookingservice.repositories.UserRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserService {
    @Autowired
    UserRepository userRepository;

    ModelMapper modelMapper = new ModelMapper();

    public List<UserDTO> findAll() {
        return Arrays.asList(modelMapper.map(userRepository.findAll(), UserDTO[].class));
    }

    public User add(User user) { return userRepository.saveAndFlush(user); }

    public void delete(User user) {
        userRepository.delete(user);
    }

    public User edit(User user) {
        return userRepository.saveAndFlush(user);
    }

    public User findByLogin(String login) { return userRepository.findByLogin(login); }
}
