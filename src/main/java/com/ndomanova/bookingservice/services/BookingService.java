package com.ndomanova.bookingservice.services;

import com.ndomanova.bookingservice.dtos.BookingDescriptionDTO;
import com.ndomanova.bookingservice.entities.Booking;
import com.ndomanova.bookingservice.repositories.BookingRepository;
import com.ndomanova.bookingservice.repositories.BookingDescriptionRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

@Service
public class BookingService {
    @Autowired
    BookingDescriptionRepository bookingDescriptionRepository;
    @Autowired
    BookingRepository bookingRepository;

    ModelMapper modelMapper = new ModelMapper();

    public Booking add(Booking booking) { return bookingRepository.saveAndFlush(booking); }

    public List<BookingDescriptionDTO> findBookings(Date startDate, Date endDate) {
        return Arrays.asList(modelMapper.map(bookingDescriptionRepository.findByStartTimeBetweenAndEndTimeBetween(startDate,
                endDate, startDate, endDate), BookingDescriptionDTO[].class));
    }

    public List<BookingDescriptionDTO> findBookings(String room, Date startDate, Date endDate) {
        return Arrays.asList(modelMapper.map(bookingDescriptionRepository.findByRoomAndStartTimeBetweenAndEndTimeBetween(room,
                startDate, endDate, startDate, endDate), BookingDescriptionDTO[].class));
    }

    public List<BookingDescriptionDTO> findBookings(String name, String surname, Date startDate, Date endDate) {
        return Arrays.asList(modelMapper.map(bookingDescriptionRepository.findByNameAndSurnameAndStartTimeBetweenAndEndTimeBetween(name,
                surname, startDate, endDate, startDate, endDate), BookingDescriptionDTO[].class));
    }
}
