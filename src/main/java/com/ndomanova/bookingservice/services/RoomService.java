package com.ndomanova.bookingservice.services;

import com.ndomanova.bookingservice.dtos.RoomDTO;
import com.ndomanova.bookingservice.entities.Room;
import com.ndomanova.bookingservice.entities.Room;
import com.ndomanova.bookingservice.repositories.RoomRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

@Service
public class RoomService {
    @Autowired
    RoomRepository roomRepository;

    ModelMapper modelMapper = new ModelMapper();
    
    public List<RoomDTO> findAll() { return Arrays.asList(modelMapper.map(roomRepository.findAll(), RoomDTO[].class)); }

    public Room add(Room room) { return roomRepository.saveAndFlush(room); }

    public void delete(Room room) { roomRepository.delete(room); }

    public Room edit(Room room) { return roomRepository.saveAndFlush(room); }

    public Room findByName(String name) { return roomRepository.findByName(name); }
}
