package com.ndomanova.bookingservice.entities;

public class SimpleUser {

    private String name;
    private String surname;
    private String login;

    public SimpleUser() {}

    public SimpleUser(User user) {
        this.name = user.getName();
        this.surname = user.getSurname();
        this.login = user.getLogin();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }
}
