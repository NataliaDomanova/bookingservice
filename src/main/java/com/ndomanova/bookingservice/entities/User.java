package com.ndomanova.bookingservice.entities;


import org.hibernate.annotations.ColumnTransformer;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.Min;

@Entity
@Table(name = "user")
public class User {
    @Id
    @Column(name = "id_user", unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "name", nullable = false, length = 50)
    private String name;
    @Column(name = "surname", nullable = false, length = 50)
    private String surname;
    @Column(name = "login", nullable = false, length = 100)
    private String login;
    @ColumnTransformer(
            read = "TRIM(CHAR(0) FROM UTF8TOSTRING(DECRYPT('AES', HASH('SHA256', STRINGTOUTF8('serret-koy-13395'), 1), password)))",
            write = "ENCRYPT('AES', HASH('SHA256', STRINGTOUTF8('serret-koy-13395'), 1), STRINGTOUTF8(?))"
    )
    @Column(name = "password", nullable = false)
    @Length(min = 6, max = 100)
    private String password;

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public User copyNotNullFields(User user) {
        this.login = user.login != null ? user.login : this.login;
        this.name = user.name != null ? user.name : this.name;
        this.surname = user.surname != null ? user.surname : this.surname;
        this.password = user.password != null ? user.password : this.password;

        return this;
    }
}