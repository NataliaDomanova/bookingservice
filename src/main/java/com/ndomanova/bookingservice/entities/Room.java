package com.ndomanova.bookingservice.entities;


import javax.persistence.*;
import javax.validation.constraints.Max;

@Entity
@Table(name = "room")
public class Room {
    @Id
    @Column(name = "id", unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "name", unique = true, nullable = false, length = 50)
    private String name;
    @Column(name = "location")
    private String location;
    @Column(name = "number_of_seats", nullable = false)
    @Max(100)
    private Integer numberOfSeats;
    @Column(name = "projector")
    private Boolean hasProjector = false;
    @Column(name = "phone", length = 100)
    private String phoneNumber;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Integer getNumberOfSeats() {
        return numberOfSeats;
    }

    public void setNumberOfSeats(Integer numberOfSeats) {
        this.numberOfSeats = numberOfSeats;
    }

    public Boolean getHasProjector() {
        return hasProjector;
    }

    public void setHasProjector(Boolean hasProjector) {
        this.hasProjector = hasProjector;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Room copyNotNullFields(Room room) {
        this.name = room.name != null ? room.name : this.name;
        this.location = room.location != null ? room.location : this.location;
        this.numberOfSeats = room.numberOfSeats != null ? room.numberOfSeats : this.numberOfSeats;
        this.hasProjector = room.hasProjector != null ? room.hasProjector : this.hasProjector;
        this.phoneNumber = room.phoneNumber != null ? room.phoneNumber : this.phoneNumber;

        return this;
    }
}