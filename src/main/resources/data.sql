drop table if exists user;

create table user (
  id_user      int primary key auto_increment not null,
  name     varchar(50)  not null,
  surname  varchar(50)  not null,
  login    varchar(100) not null,
  password varchar(100) not null
);

insert into user values (1, 'John', 'Smith', 'jsmith',
ENCRYPT('AES', HASH('SHA256', STRINGTOUTF8('serret-koy-13395'), 1), STRINGTOUTF8('qwerty')));
insert into user values (2,  'Jane', 'Doe', 'jdoe',
ENCRYPT('AES', HASH('SHA256', STRINGTOUTF8('serret-koy-13395'), 1), STRINGTOUTF8('mySecret')));

drop table if exists room;

create table room (
  id              int AUTO_INCREMENT primary key not null,
  name            varchar(50) not null,
  location        varchar(256),
  number_of_seats int         not null,
  projector       boolean default false,
  phone           varchar(100)
);

insert into room values (1, 'Large Room', '1st floor', 10, true,'22-22-22-22');
insert into room values (2, 'Medium Room', '1st floor', 6, true, null);
insert into room values (3, 'Small Room', '2nd floor', 4,	false, null);

drop table if exists booking;

create table booking (
  id         int primary key auto_increment not null,
  id_user    int                      not null,
  id_room    int                      not null,
  start_time datetime                 not null,
  end_time   datetime                 not null,
  foreign key (id_user) references user(id_user),
  foreign key (id_room) references room(id)
);

drop view if exists booking_view;

create view booking_view as
select rownum as id, r.name as room, u.name, u.surname, b.start_time, b.end_time
from room r, user u, booking b
where b.id_room = r.id and b.id_user = u.id_user;


