package com.ndomanova.bookingservice;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ndomanova.bookingservice.dtos.UserDTO;
import com.ndomanova.bookingservice.entities.User;
import junit.framework.TestCase;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.io.IOException;

import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = BookingServiceApplication.class)
@WebAppConfiguration
public class UserControllerTest extends AbstractTest {
    protected MockMvc mvc;
    @Autowired
    WebApplicationContext webApplicationContext;
    User user;
    @Before
    public void setUp() {
        mvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
        user = new User();
        user.setName("natalia");
        user.setSurname("domanova");
        user.setLogin("ndomanova");
        user.setPassword("helloworld");
    }

    @Test
    public void getAllUsersTest() throws Exception {
        String uri = "/users";
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)
                .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        int status = mvcResult.getResponse().getStatus();
        String content = mvcResult.getResponse().getContentAsString();
        UserDTO[] users = mapFromJson(content, UserDTO[].class);


        assertEquals(200, status);
        TestCase.assertEquals(2, users.length);

        assertEquals("John", users[0].getName());
        assertEquals("Smith", users[0].getSurname());
        assertEquals("jsmith", users[0].getLogin());

        assertEquals("Jane", users[1].getName());
        assertEquals("Doe", users[1].getSurname());
        assertEquals("jdoe", users[1].getLogin());
    }

    @Test
    public void addUserTestIdenticalLogin() throws Exception {
        String uri = "/users";
        user.setLogin("jsmith");
        String inputJson = mapToJson(user);
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(400, status);
        String content = mvcResult.getResponse().getContentAsString();
        assertEquals( "user is already created", content);
    }

    @Test
    public void addUserTestInvalidFields() throws Exception {
        String uri = "/users";
        user.setPassword("pol");
        String inputJson = mapToJson(user);
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(400, status);
    }

    @Test
    public void addUserTest() throws Exception {
        String uri = "/users";
        String inputJson = mapToJson(user);
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(201, status);
        String content = mvcResult.getResponse().getContentAsString();
        assertEquals( "user is created successfully", content);

        UserDTO[] users = getUsersArray();
        assertEquals(3, users.length);

        assertEquals("John", users[0].getName());
        assertEquals("Smith", users[0].getSurname());
        assertEquals("jsmith", users[0].getLogin());

        assertEquals("Jane", users[1].getName());
        assertEquals("Doe", users[1].getSurname());
        assertEquals("jdoe", users[1].getLogin());

        assertEquals("natalia", users[2].getName());
        assertEquals("domanova", users[2].getSurname());
        assertEquals("ndomanova", users[2].getLogin());

        uri = "/users?login=ndomanova";
        mvc.perform(MockMvcRequestBuilders.delete(uri)).andReturn();
    }

    @Test
    public void editUserTestNoUser() throws Exception {
        String uri = "/users?login=ndomanova";
        user.setName("natalia");

        String inputJson = mapToJson(user);
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.put(uri)
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(400, status);
        String content = mvcResult.getResponse().getContentAsString();
        assertEquals("user is not found", content);
    }

    @Test
    public void editUserTest() throws Exception {
        String uri = "/users?login=jsmith";
        User user = new User();
        user.setName("Johnny");

        String inputJson = mapToJson(user);
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.put(uri)
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(200, status);
        String content = mvcResult.getResponse().getContentAsString();
        assertEquals("user is updated successfully", content);

        UserDTO[] users = getUsersArray();
        assertEquals(2, users.length);

        assertEquals("Johnny", users[0].getName());
        assertEquals("Smith", users[0].getSurname());
        assertEquals("jsmith", users[0].getLogin());

        assertEquals("Jane", users[1].getName());
        assertEquals("Doe", users[1].getSurname());
        assertEquals("jdoe", users[1].getLogin());

        uri = "/users?login=jsmith";
        user = new User();
        user.setName("John");

        inputJson = mapToJson(user);
        mvc.perform(MockMvcRequestBuilders.put(uri)
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();
    }

    @Test
    public void deleteUserTestNoUser() throws Exception {
        String uri = "/users?login=ndomanova";
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.delete(uri)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(400, status);
        String content = mvcResult.getResponse().getContentAsString();
        assertEquals("user is not found", content);
    }

    @Test
    public void deleteUserTest() throws Exception {
        String uri = "/users?login=jsmith";
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.delete(uri)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(200, status);
        String content = mvcResult.getResponse().getContentAsString();
        assertEquals("user is deleted successfully", content);

        UserDTO[] users = getUsersArray();
        assertEquals(1, users.length);

        assertEquals("Jane", users[0].getName());
        assertEquals("Doe", users[0].getSurname());
        assertEquals("jdoe", users[0].getLogin());
    }

    public UserDTO[] getUsersArray() throws Exception {
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get("/users")
                .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        String content = mvcResult.getResponse().getContentAsString();
        return mapFromJson(content, UserDTO[].class);

    }
}
