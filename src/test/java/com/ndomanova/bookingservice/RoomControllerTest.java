package com.ndomanova.bookingservice;
import com.ndomanova.bookingservice.dtos.RoomDTO;
import junit.framework.TestCase;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.io.IOException;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = BookingServiceApplication.class)
@WebAppConfiguration
public class RoomControllerTest extends AbstractTest {
    protected MockMvc mvc;
    @Autowired
    WebApplicationContext webApplicationContext;
    RoomDTO room;
    @Before
    public void setUp() {
        mvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
        room = new RoomDTO();
        room.setName("very small room");
        room.setLocation("1st floor");
        room.setHasProjector(false);
        room.setNumberOfSeats(2);
    }

    @Test
    public void getAllRoomsTest() throws Exception {
        String uri = "/rooms";
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)
                .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        int status = mvcResult.getResponse().getStatus();
        String content = mvcResult.getResponse().getContentAsString();
        RoomDTO[] rooms = mapFromJson(content, RoomDTO[].class);


        assertEquals(200, status);
        TestCase.assertEquals(3, rooms.length);

        assertEquals("Large Room", rooms[0].getName());
        assertEquals("1st floor",rooms[0].getLocation());
        assertEquals(Integer.valueOf(10), rooms[0].getNumberOfSeats());
        assertTrue(rooms[0].getHasProjector());
        assertEquals("22-22-22-22", rooms[0].getPhoneNumber());

        assertEquals("Medium Room", rooms[1].getName());
        assertEquals("1st floor",rooms[1].getLocation());
        assertEquals(Integer.valueOf(6), rooms[1].getNumberOfSeats());
        assertTrue(rooms[1].getHasProjector());
        assertNull( rooms[1].getPhoneNumber());

        assertEquals("Small Room", rooms[2].getName());
        assertEquals("2nd floor",rooms[2].getLocation());
        assertEquals(Integer.valueOf(4), rooms[2].getNumberOfSeats());
        assertFalse(rooms[2].getHasProjector());
        assertNull( rooms[2].getPhoneNumber());
    }

    @Test
    public void addRoomTestIdenticalName() throws Exception {
        String uri = "/rooms";
        room.setName("Small Room");
        String inputJson = mapToJson(room);
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(400, status);
        String content = mvcResult.getResponse().getContentAsString();
        assertEquals( "room is already created", content);

        assertEquals(3, getRoomsArray().length);
    }

    @Test
    public void addRoomTestInvalidFields() throws Exception {
        String uri = "/rooms";
        room.setNumberOfSeats(200);
        String inputJson = mapToJson(room);
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(400, status);
    }

    @Test
    public void addRoomTest() throws Exception {
        String uri = "/rooms";
        String inputJson = mapToJson(room);
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(201, status);
        String content = mvcResult.getResponse().getContentAsString();
        assertEquals( "room is created successfully", content);

        RoomDTO[] rooms = getRoomsArray();
        assertEquals(4, rooms.length);

        assertEquals("Large Room", rooms[0].getName());
        assertEquals("1st floor",rooms[0].getLocation());
        assertEquals(Integer.valueOf(10), rooms[0].getNumberOfSeats());
        assertTrue(rooms[0].getHasProjector());
        assertEquals("22-22-22-22", rooms[0].getPhoneNumber());

        assertEquals("Medium Room", rooms[1].getName());
        assertEquals("1st floor",rooms[1].getLocation());
        assertEquals(Integer.valueOf(6), rooms[1].getNumberOfSeats());
        assertTrue(rooms[1].getHasProjector());
        assertNull( rooms[1].getPhoneNumber());

        assertEquals("Small Room", rooms[2].getName());
        assertEquals("2nd floor",rooms[2].getLocation());
        assertEquals(Integer.valueOf(4), rooms[2].getNumberOfSeats());
        assertFalse(rooms[2].getHasProjector());
        assertNull( rooms[2].getPhoneNumber());

        assertEquals("very small room", rooms[3].getName());
        assertEquals("1st floor",rooms[3].getLocation());
        assertEquals(Integer.valueOf(2), rooms[3].getNumberOfSeats());
        assertFalse(rooms[3].getHasProjector());
        assertNull( rooms[3].getPhoneNumber());

        uri = "/rooms?name=very small room";
        mvcResult = mvc.perform(MockMvcRequestBuilders.delete(uri)).andReturn();
    }

    @Test
    public void editRoomTestNoRoomDTO() throws Exception {
        String uri = "/rooms?name=someroom";
        room.setName("very small room");

        String inputJson = mapToJson(room);
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.put(uri)
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(400, status);
        String content = mvcResult.getResponse().getContentAsString();
        assertEquals("room is not found", content);
    }

    @Test
    public void editRoomTest() throws Exception {
        String uri = "/rooms?name=Small Room";
        RoomDTO room = new RoomDTO();
        room.setName("Not So Small Room");
        room.setHasProjector(true);

        String inputJson = mapToJson(room);
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.put(uri)
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(200, status);
        String content = mvcResult.getResponse().getContentAsString();
        assertEquals("room is updated successfully", content);

        RoomDTO[] rooms = getRoomsArray();
        assertEquals(3, rooms.length);

        assertEquals("Large Room", rooms[0].getName());
        assertEquals("1st floor",rooms[0].getLocation());
        assertEquals(Integer.valueOf(10), rooms[0].getNumberOfSeats());
        assertTrue(rooms[0].getHasProjector());
        assertEquals("22-22-22-22", rooms[0].getPhoneNumber());

        assertEquals("Medium Room", rooms[1].getName());
        assertEquals("1st floor",rooms[1].getLocation());
        assertEquals(Integer.valueOf(6), rooms[1].getNumberOfSeats());
        assertTrue(rooms[1].getHasProjector());
        assertNull( rooms[1].getPhoneNumber());

        assertEquals("Not So Small Room", rooms[2].getName());
        assertEquals("2nd floor",rooms[2].getLocation());
        assertEquals(Integer.valueOf(4), rooms[2].getNumberOfSeats());
        assertTrue(rooms[2].getHasProjector());
        assertNull( rooms[2].getPhoneNumber());

        uri = "/rooms?name=Not So Small Room";
        room.setName("Small Room");
        room.setHasProjector(false);

        inputJson = mapToJson(room);
        mvc.perform(MockMvcRequestBuilders.put(uri)
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();
    }

    @Test
    public void deleteRoomTestNoRoom() throws Exception {
        String uri = "/rooms?name=someroom";
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.delete(uri)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(400, status);
        String content = mvcResult.getResponse().getContentAsString();
        assertEquals("room is not found", content);
        assertEquals(3, getRoomsArray().length);
    }

    @Test
    public void deleteRoomTest() throws Exception {
        String uri = "/rooms?name=Medium Room";
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.delete(uri)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(200, status);
        String content = mvcResult.getResponse().getContentAsString();
        assertEquals("room is deleted successfully", content);

        RoomDTO[] rooms = getRoomsArray();
        assertEquals(2, rooms.length);

        assertEquals("Large Room", rooms[0].getName());
        assertEquals("1st floor",rooms[0].getLocation());
        assertEquals(Integer.valueOf(10), rooms[0].getNumberOfSeats());
        assertTrue(rooms[0].getHasProjector());
        assertEquals("22-22-22-22", rooms[0].getPhoneNumber());

        assertEquals("Small Room", rooms[1].getName());
        assertEquals("2nd floor",rooms[1].getLocation());
        assertEquals(Integer.valueOf(4), rooms[1].getNumberOfSeats());
        assertFalse(rooms[1].getHasProjector());
        assertNull( rooms[1].getPhoneNumber());


    }

    public RoomDTO[] getRoomsArray() throws Exception {
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get("/rooms")
                .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        String content = mvcResult.getResponse().getContentAsString();
        return mapFromJson(content, RoomDTO[].class);
    }
}
