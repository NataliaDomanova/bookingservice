package com.ndomanova.bookingservice;

import com.ndomanova.bookingservice.dtos.BookingDescriptionDTO;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = BookingServiceApplication.class)
@WebAppConfiguration
public class BookingControllerTest extends AbstractTest {
    protected MockMvc mvc;
    @Autowired
    WebApplicationContext webApplicationContext;

    @Before
    public void setUp() {
        mvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    @Test
    public void makeBookingTestInconsistentDates() throws Exception {
        String uri = "/booking?login=jsmith&password=qwerty&room=Medium Room" +
                "&start_time=2002-02-02 10:10:10&end_time=2002-02-01 10:11:10";
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri)
                .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        int status = mvcResult.getResponse().getStatus();
        assertEquals(400, status);
        String content = mvcResult.getResponse().getContentAsString();

        assertEquals("inconsistent dates", content);
    }

    @Test
    public void makeBookingTestNoUser() throws Exception {
        String uri = "/booking?login=ndomanova&password=qwerty&room=&Medium Room" +
                "&start_time=2002-02-02 10:10:10&end_time=2002-02-03 10:11:10";
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri)
                .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        int status = mvcResult.getResponse().getStatus();
        assertEquals(400, status);
        String content = mvcResult.getResponse().getContentAsString();

        assertEquals("no user found", content);
    }

    @Test
    public void makeBookingTestInconsistentCredentials() throws Exception {
        String uri = "/booking?login=jsmith&password=helloworld&room=Medium Room" +
                "&start_time=2002-02-02 10:10:10&end_time=2002-02-03 10:11:10";
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri)
                .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        int status = mvcResult.getResponse().getStatus();
        assertEquals(400, status);
        String content = mvcResult.getResponse().getContentAsString();

        assertEquals("incorrect password", content);
    }

    @Test
    public void makeBookingTestNoRoom() throws Exception {
        String uri = "/booking/?login=jsmith&password=qwerty&room=Some Strange Room" +
                "&start_time=2002-02-02 10:10:10&end_time=2002-02-03 10:11:10";
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri)
                .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        int status = mvcResult.getResponse().getStatus();
        assertEquals(400, status);
        String content = mvcResult.getResponse().getContentAsString();

        assertEquals("no room found", content);
    }

    @Test
    public void makeBookingTest() throws Exception {
        String uri = "/booking/?login=jsmith&password=qwerty&room=Medium Room" +
                "&start_time=2002-02-02 10:10:10&end_time=2002-02-03 10:11:10";
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri)
                .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        int status = mvcResult.getResponse().getStatus();
        assertEquals(201, status);
        String content = mvcResult.getResponse().getContentAsString();

        assertEquals("room was booked successfully", content);
    }

    @Test
    public void getBookingByTimeFrameTestAllRecords() throws Exception {
        makeBookings();
        String uri = "/booking?start_time=2002-01-01 10:10:10&end_time=2002-05-02 11:10:10";
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)
                .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        int status = mvcResult.getResponse().getStatus();
        String content = mvcResult.getResponse().getContentAsString();
        BookingDescriptionDTO[] bookings = mapFromJson(content, BookingDescriptionDTO[].class);

        assertEquals(200, status);

        assertEquals("Medium Room", bookings[0].getRoom());
        assertEquals("John", bookings[0].getName());
        assertEquals("Smith", bookings[0].getSurname());
        assertEquals("Sat Feb 02 10:10:10 CET 2002", bookings[0].getStartTime().toString());
        assertEquals("Sat Feb 02 11:10:10 CET 2002", bookings[0].getEndTime().toString());

        assertEquals("Small Room", bookings[1].getRoom());
        assertEquals("John", bookings[1].getName());
        assertEquals("Smith", bookings[1].getSurname());
        assertEquals("Sat Mar 02 14:10:10 CET 2002", bookings[1].getStartTime().toString());
        assertEquals("Sat Mar 02 17:10:10 CET 2002", bookings[1].getEndTime().toString());

        assertEquals("Large Room", bookings[2].getRoom());
        assertEquals("Jane", bookings[2].getName());
        assertEquals("Doe", bookings[2].getSurname());
        assertEquals("Sat Feb 02 10:10:10 CET 2002", bookings[2].getStartTime().toString());
        assertEquals("Sat Feb 02 11:10:10 CET 2002", bookings[2].getEndTime().toString());
    }

    @Test
    public void getBookingByTimeFrameTestNoRecords() throws Exception {
        makeBookings();
        String uri = "/booking?start_time=2003-01-01 10:10:10&end_time=2003-05-02 11:10:10";
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)
                .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        int status = mvcResult.getResponse().getStatus();
        String content = mvcResult.getResponse().getContentAsString();
        BookingDescriptionDTO[] bookings = mapFromJson(content, BookingDescriptionDTO[].class);

        assertEquals(200, status);
        assertEquals(0, bookings.length);
    }

    @Test
    public void getBookingByTimeFrameTest() throws Exception {
        makeBookings();
        String uri = "/booking?start_time=2002-01-01 01:01:10&end_time=2002-02-02 21:10:10";
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)
                .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        int status = mvcResult.getResponse().getStatus();
        String content = mvcResult.getResponse().getContentAsString();
        BookingDescriptionDTO[] bookings = mapFromJson(content, BookingDescriptionDTO[].class);

        assertEquals(200, status);

        assertEquals("Medium Room", bookings[0].getRoom());
        assertEquals("John", bookings[0].getName());
        assertEquals("Smith", bookings[0].getSurname());
        assertEquals("Sat Feb 02 10:10:10 CET 2002", bookings[0].getStartTime().toString());
        assertEquals("Sat Feb 02 11:10:10 CET 2002", bookings[0].getEndTime().toString());

        assertEquals("Large Room", bookings[1].getRoom());
        assertEquals("Jane", bookings[1].getName());
        assertEquals("Doe", bookings[1].getSurname());
        assertEquals("Sat Feb 02 10:10:10 CET 2002", bookings[1].getStartTime().toString());
        assertEquals("Sat Feb 02 11:10:10 CET 2002", bookings[1].getEndTime().toString());
    }

    @Test
    public void getBookingByRoomAndTimeFrameTest() throws Exception {
        makeBookings();
        String uri = "/booking/room?room=Medium Room&start_time=2002-01-01 10:10:10&end_time=2002-05-02 11:10:10";
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)
                .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        int status = mvcResult.getResponse().getStatus();
        String content = mvcResult.getResponse().getContentAsString();
        assertEquals(200, status);

        BookingDescriptionDTO[] bookings = mapFromJson(content, BookingDescriptionDTO[].class);

        assertEquals("Medium Room", bookings[0].getRoom());
        assertEquals("John", bookings[0].getName());
        assertEquals("Smith", bookings[0].getSurname());
        assertEquals("Sat Feb 02 10:10:10 CET 2002", bookings[0].getStartTime().toString());
        assertEquals("Sat Feb 02 11:10:10 CET 2002", bookings[0].getEndTime().toString());
    }

    @Test
    public void getBookingByUserAndTimeFrameTest() throws Exception {
        makeBookings();
        String uri = "/booking/user?name=Jane&surname=Doe&start_time=2000-02-02 21:01:01&end_time=2008-02-02 01:10:10";
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)
                .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        int status = mvcResult.getResponse().getStatus();
        String content = mvcResult.getResponse().getContentAsString();
        assertEquals(200, status);

        BookingDescriptionDTO[] bookings = mapFromJson(content, BookingDescriptionDTO[].class);

        assertEquals("Large Room", bookings[0].getRoom());
        assertEquals("Jane", bookings[0].getName());
        assertEquals("Doe", bookings[0].getSurname());
        assertEquals("Sat Feb 02 10:10:10 CET 2002", bookings[0].getStartTime().toString());
        assertEquals("Sat Feb 02 11:10:10 CET 2002", bookings[0].getEndTime().toString());
    }

    @Test
    public void getBookingByUserAndTimeFrameTestIncorrectDateFormat() throws Exception {
        makeBookings();
        String uri = "/booking/user?name=Jane&surname=Doe&start_time=2000:02:02 01:01:01&end_time=2008-02-02 21:10:10";
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)
                .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        int status = mvcResult.getResponse().getStatus();
        String content = mvcResult.getResponse().getContentAsString();
        assertEquals(200, status);

        BookingDescriptionDTO[] bookings = mapFromJson(content, BookingDescriptionDTO[].class);
        assertEquals(0, bookings.length);
    }

    public void makeBookings() throws Exception {
        String uri = "/booking?login=jsmith&password=qwerty&room=Medium Room&start_time=2002-02-02 10:10:10" +
                "&end_time=2002-02-02 11:10:10";
       mvc.perform(MockMvcRequestBuilders.post(uri)
                .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        uri = "/booking?login=jsmith&password=qwerty&room=Small Room&start_time=2002-03-02 14:10:10&" +
                "end_time=2002-03-02 17:10:10";
        mvc.perform(MockMvcRequestBuilders.post(uri)
                .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        uri = "/booking?login=jdoe&password=mySecret&room=Large Room&start_time=2002-02-02 10:10:10&" +
                "end_time=2002-02-02 11:10:10";
        mvc.perform(MockMvcRequestBuilders.post(uri)
                .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
    }


    public BookingDescriptionDTO[] getBookingsArray() throws Exception {
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get("/booking")
                .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        String content = mvcResult.getResponse().getContentAsString();
        return mapFromJson(content, BookingDescriptionDTO[].class);

    }

}
